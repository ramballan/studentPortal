package ru.netcracker.studportal.primebeans;

import ru.netcracker.studportal.controllers.FacultyController;
import ru.netcracker.studportal.entity.Faculty;
import ru.netcracker.studportal.entity.Student;
import ru.netcracker.studportal.controllers.StudentController;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@SessionScoped
public class StudentTableView implements Serializable {

    private List<Student> students;
    private List<Student> filteredStudents;
    private List<Faculty> allFaculties;
    private Faculty selectedFaculty;
    private Student selectedStudent;

    @ManagedProperty("#{facultyController}")
    private FacultyController facultyController;

    @ManagedProperty("#{studentController}")
    private StudentController studentController;

    @PostConstruct
    public void init() {
        students = studentController.getStudentList();
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudentController(StudentController studentController) {
        this.studentController = studentController;
    }

    public void setFacultyController(FacultyController facultyController) {
        this.facultyController = facultyController;
    }
    public void addStudent(){
        selectedStudent = new Student();
        allFaculties = facultyController.getFacultyList();
        if(selectedFaculty==null) selectedFaculty = allFaculties.get(0);
    }
    public void editStudent(){
        allFaculties = facultyController.getFacultyList();
        selectedFaculty = selectedStudent.getStudentGroup().getFaculty();
    }
    public void deleteStudent(){
        if (selectedStudent!=null){
            studentController.deleteStudent(selectedStudent);
            students = studentController.getStudentList();
        }
    }
    public void saveOrUpdate(){
        studentController.saveOrUpdateStudent(selectedStudent);
        students = studentController.getStudentList();
    }
    public List<Student> getFilteredStudents() {
        return filteredStudents;
    }

    public void setFilteredStudents(List<Student> filteredStudents) {
        this.filteredStudents = filteredStudents;
    }

    public Student getSelectedStudent() {
        return selectedStudent;
    }

    public void setSelectedStudent(Student selectedStudent) {
        this.selectedStudent = selectedStudent;
    }


    public Faculty getSelectedFaculty() {
        return selectedFaculty;
    }

    public void setSelectedFaculty(Faculty selectedFaculty) {
        this.selectedFaculty = selectedFaculty;
    }

    public List<Faculty> getAllFaculties() {
        return allFaculties;
    }
}