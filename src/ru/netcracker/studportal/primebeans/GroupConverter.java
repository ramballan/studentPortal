package ru.netcracker.studportal.primebeans;

import ru.netcracker.studportal.entity.Group;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.List;

@FacesConverter("groupConverter")
public class GroupConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if(s != null && s.trim().length() > 0) {
            StudentTableView stv = (StudentTableView) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("studentTableView");
            List<Group> groupList = stv.getSelectedFaculty().getGroupsList();
            for (Group group : groupList) {
                if (group.getGroupId()==Long.valueOf(s)){
                    return group;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if(o != null) {
            return String.valueOf(((Group) o).getGroupId());
        }
        else {
            return null;
        }
    }
}
