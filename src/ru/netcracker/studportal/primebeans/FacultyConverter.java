package ru.netcracker.studportal.primebeans;

import ru.netcracker.studportal.entity.Faculty;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.List;

@FacesConverter("facultyConverter")
public class FacultyConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if(s != null && s.trim().length() > 0) {
            StudentTableView stv = (StudentTableView) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("studentTableView");
            List<Faculty> facultyList = stv.getAllFaculties();
            for (Faculty faculty : facultyList) {
                if (faculty.getFacultyId()==Long.valueOf(s)){
                    return faculty;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if(o != null) {
            return String.valueOf(((Faculty) o).getFacultyId());
        }
        else {
            return null;
        }
    }
}
