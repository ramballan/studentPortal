/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.netcracker.studportal.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import ru.netcracker.studportal.entity.Professor;
import ru.netcracker.studportal.entity.Subject;
import ru.netcracker.studportal.entity.Faculty;
import ru.netcracker.studportal.entity.Group;
import ru.netcracker.studportal.entity.Student;



public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure().addAnnotatedClass(Student.class)
                                    .addAnnotatedClass(Group.class)
                                    .addAnnotatedClass(Faculty.class)
                                    .addAnnotatedClass(Professor.class)
                                    .addAnnotatedClass(Subject.class);
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
                    configuration.getProperties()). buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
