package ru.netcracker.studportal.db;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.netcracker.studportal.entity.Faculty;
import ru.netcracker.studportal.entity.Group;
import ru.netcracker.studportal.entity.Student;
import ru.netcracker.studportal.entity.Subject;


import java.util.ArrayList;
import java.util.List;

public class DataHelper {
    private SessionFactory sessionFactory = null;
    private static DataHelper dataHelper;

    private DataHelper(){
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static  DataHelper getInstance(){
        return dataHelper == null ? new DataHelper() : dataHelper;
    }

    private Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public List<Student> getAllStudents() {
        Session session = null;
        List<Student> studentList = new ArrayList<Student>();
        try {
            session = getSession();
            session.beginTransaction();
            Query query = session.createQuery("FROM Student");
            studentList =(List<Student>) query.list();
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            return studentList;
        }
    }

    public List<Subject> getAllSubjects() {
        Session session = null;
        List<Subject> subjectList = new ArrayList<Subject>();
        try {
            session = getSession();
            session.beginTransaction();
            Query query = session.createQuery("FROM Subject");
            subjectList =(List<Subject>) query.list();
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            return subjectList;
        }
    }

    public List<Group> getAllGroups() {
        Session session = null;
        List<Group> groupList = new ArrayList<Group>();
        try {
            session = getSession();
            session.beginTransaction();
            groupList =session.createCriteria(Group.class).list();
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            return groupList;
        }
    }
    public List<Faculty> getAllFaculties() {
        Session session = null;
        List<Faculty> facultyList = new ArrayList<Faculty>();
        try {
            session = getSession();
            session.beginTransaction();
            Query query = session.createQuery("FROM Faculty");
            facultyList =(List<Faculty>) query.list();
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            return facultyList;
        }
    }
    public void saveOrUpdateStudent(Student student){
        Session session = null;
        try {
            session = getSession();
            session.beginTransaction();
            session.saveOrUpdate(student);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
    public void deleteStudent(Student student){
        Session session = null;
        try {
            session = getSession();
            session.beginTransaction();
            session.delete(student);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
