package ru.netcracker.studportal.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class Professor {
    private Long id;
    private String surname;
    private String name;
    private String middleName;
    private ArrayList<Subject> subjectList = new ArrayList<Subject>();


    public ArrayList<Subject> getSubjectList() {
        return subjectList;
    }

    public String getAllSubjectsAsString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < subjectList.size(); i++) {
            Subject sub = subjectList.get(i);
            if (i==0)sb.append(sub.getSubjectName());
            else sb.append(", ").append(sub.getSubjectName());
        }
        return sb.toString();
    }

    public void setSubjectList(ArrayList<Subject> subjectList) {
        this.subjectList = subjectList;
    }
    public void addSubject(Subject sub){
        subjectList.add(sub);
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
