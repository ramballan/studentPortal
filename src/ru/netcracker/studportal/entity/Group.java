package ru.netcracker.studportal.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by user on 11.09.2016.
 */
@Entity
@Table(name = "groups", schema = "", catalog = "studentdb")
public class Group {
    private long groupId;
    private String shortDescription;
    private String specialty;
    private Faculty faculty;
    private List<Student> studentsList;
    private List<Subject> subjectsList;

    @Id
    @Column(name = "groupId")
    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "shortDescription")
    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @Basic
    @Column(name = "specialty")
    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group that = (Group) o;

        if (groupId != that.groupId) return false;
        if (shortDescription != null ? !shortDescription.equals(that.shortDescription) : that.shortDescription != null)
            return false;
        if (specialty != null ? !specialty.equals(that.specialty) : that.specialty != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (groupId ^ (groupId >>> 32));
        result = 31 * result + (shortDescription != null ? shortDescription.hashCode() : 0);
        result = 31 * result + (specialty != null ? specialty.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "facultyId", referencedColumnName = "facultyId", nullable = false)
    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @OneToMany(mappedBy = "studentGroup")
    public List<Student> getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(List<Student> studentsList) {
        this.studentsList = studentsList;
    }

    @ManyToMany
    @JoinTable(name = "grsub", catalog = "studentdb", schema = "", joinColumns = @JoinColumn(name = "groupID", referencedColumnName = "groupId"), inverseJoinColumns = @JoinColumn(name = "subjectID", referencedColumnName = "subjectID"))
    public List<Subject> getSubjectsList() {
        return subjectsList;
    }

    public void setSubjectsList(List<Subject> subjectsList) {
        this.subjectsList = subjectsList;
    }

    @Override
    public String toString() {
        return shortDescription + " - " + specialty;
    }
}
