package ru.netcracker.studportal.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 11.09.2016.
 */
@Entity
@Table(name = "subjects", schema = "", catalog = "studentdb")
public class Subject {
    private long subjectId;
    private String subjectName;
    private Set<Professor> professorsSet = new HashSet<Professor>();
    private Set<Group> groupsSet = new HashSet<Group>();

    @Id
    @Column(name = "subjectID")
    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    @Basic
    @Column(name = "subjectName")
    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject that = (Subject) o;

        if (subjectId != that.subjectId) return false;
        if (subjectName != null ? !subjectName.equals(that.subjectName) : that.subjectName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (subjectId ^ (subjectId >>> 32));
        result = 31 * result + (subjectName != null ? subjectName.hashCode() : 0);
        return result;
    }
    @ManyToMany(mappedBy = "profSubjectSet")
    public Set<Professor> getProfessorsList() {
        return professorsSet;
    }

    public void setProfessorsList(Set<Professor> professorsSet) {
        this.professorsSet = professorsSet;
    }

    @ManyToMany(mappedBy = "subjectsList")
    public Set<Group> getGroupsSet() {
        return groupsSet;
    }

    public void setGroupsSet(Set<Group> groupsSet) {
        this.groupsSet = groupsSet;
    }

}
