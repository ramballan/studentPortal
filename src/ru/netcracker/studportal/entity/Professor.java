package ru.netcracker.studportal.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 11.09.2016.
 */
@Entity
@Table(name = "professors", schema = "", catalog = "studentdb")
public class Professor {
    private long professorId;
    private String surname;
    private String name;
    private String middlename;
    private Set<Subject> profSubjectSet = new HashSet<Subject>();

    @Id
    @Column(name = "professorId")
    public long getProfessorId() {
        return professorId;
    }

    public void setProfessorId(long professorId) {
        this.professorId = professorId;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "middlename")
    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Professor that = (Professor) o;

        if (professorId != that.professorId) return false;
        if (middlename != null ? !middlename.equals(that.middlename) : that.middlename != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (surname != null ? !surname.equals(that.surname) : that.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (professorId ^ (professorId >>> 32));
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (middlename != null ? middlename.hashCode() : 0);
        return result;
    }
    @ManyToMany
    @JoinTable(name = "profsub", catalog = "studentdb", schema = "", joinColumns = @JoinColumn(name = "professorID", referencedColumnName = "professorId", nullable = false), inverseJoinColumns = @JoinColumn(name = "subjectID", referencedColumnName = "subjectID", nullable = false))
    public Set<Subject> getProfSubjectSet() {
        return profSubjectSet;
    }
    public void setProfSubjectSet(Set<Subject> profSubjectSet) {
        this.profSubjectSet = profSubjectSet;
    }
}
