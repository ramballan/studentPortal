package ru.netcracker.studportal.controllers;

import ru.netcracker.studportal.db.DataHelper;
import ru.netcracker.studportal.entity.Faculty;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean(eager = true)
@SessionScoped
public class FacultyController {
    private List<Faculty> facultyList;

    public FacultyController() {
        facultyList = getAllFaculties();
    }

    public List<Faculty> getAllFaculties(){
        return DataHelper.getInstance().getAllFaculties();
    }

    public List<Faculty> getFacultyList() {
        return facultyList;
    }
}
