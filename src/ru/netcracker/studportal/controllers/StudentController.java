package ru.netcracker.studportal.controllers;

import ru.netcracker.studportal.db.DataHelper;
import ru.netcracker.studportal.entity.Student;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean(eager = true)
@SessionScoped
public class StudentController implements Serializable{
    private List<Student> studentList;
    public StudentController() {
        getAllStudent();
    }
    public void saveOrUpdateStudent(Student student){
        DataHelper.getInstance().saveOrUpdateStudent(student);
        getAllStudent();
    }
    public void deleteStudent(Student student){
        DataHelper.getInstance().deleteStudent(student);
        getAllStudent();
    }

    public void getAllStudent(){
        studentList = DataHelper.getInstance().getAllStudents();
    }

    public List<Student> getStudentList() {
        return studentList;
    }
}
