package ru.netcracker.studportal.controllers;

import ru.netcracker.studportal.db.DataHelper;
import ru.netcracker.studportal.entity.Group;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean(eager = true)
@SessionScoped
public class GroupController {
    private List<Group> groupList;

    public void getAllSubjects(){
        groupList = DataHelper.getInstance().getAllGroups();
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }
}
