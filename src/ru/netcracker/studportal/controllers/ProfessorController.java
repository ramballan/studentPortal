package ru.netcracker.studportal.controllers;

import ru.netcracker.studportal.beans.Professor;
import ru.netcracker.studportal.beans.Subject;
import ru.netcracker.studportal.db.Database;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@ManagedBean(eager = true)
@SessionScoped
public class ProfessorController implements Serializable{
    private ArrayList<Professor> professorList;
    public ProfessorController() {
        professorList = new ArrayList<Professor>();
        getAllProfessor();
        getProfessorsSubjects();
    }
    public void getAllProfessor(){
        String sql = "select * from professors";
        getProfessorBySQL(sql);
    }
    private void getProfessorBySQL(String sql){
        Statement stmt = null;
        ResultSet rs = null;
        Connection connection = null;
        try{
            connection = Database.getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()){
                Professor prof = new Professor();
                prof.setId(rs.getLong("professorId"));
                prof.setSurname(rs.getString("surname"));
                prof.setName(rs.getString("name"));
                prof.setMiddleName(rs.getString("middlename"));
                professorList.add(prof);
            }
        } catch (SQLException e) {
                Logger.getLogger(ProfessorController.class.getName()).log(Level.SEVERE, null, e);
        }finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                Logger.getLogger(ProfessorController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    public void getProfessorsSubjects(){
        for (Professor professor : professorList) {
            Statement stmt = null;
            ResultSet rs = null;
            Connection connection = null;
            try{
                connection = Database.getConnection();
                stmt = connection.createStatement();
                rs = stmt.executeQuery("SELECT * FROM profsub as ps " +
                        "inner join subjects sub " +
                        "where ps.professorID="+professor.getId()+" and ps.subjectID=sub.subjectID;");
                while (rs.next()){
                    Subject sub = new Subject();
                    sub.setId(rs.getLong("subjectID"));
                    sub.setSubjectName(rs.getString("subjectName"));
                    professor.addSubject(sub);
                }
            } catch (SQLException e) {
                Logger.getLogger(ProfessorController.class.getName()).log(Level.SEVERE, null, e);
            }finally {
                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    Logger.getLogger(ProfessorController.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    public void updateData(){
        professorList.clear();
        getAllProfessor();
        getProfessorsSubjects();
    }

    public ArrayList<Professor> getProfessorList() {
        return professorList;
    }
}
