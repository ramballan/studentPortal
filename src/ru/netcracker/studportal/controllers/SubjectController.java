package ru.netcracker.studportal.controllers;


import ru.netcracker.studportal.db.DataHelper;
import ru.netcracker.studportal.entity.Subject;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean(eager = true)
@SessionScoped
public class SubjectController {
    private List<Subject> subjectList;

    public SubjectController() {
        getAllSubjects();
    }

    public void getAllSubjects(){
        subjectList = DataHelper.getInstance().getAllSubjects();
    }

    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<Subject> subjectList) {
        this.subjectList = subjectList;
    }
}
